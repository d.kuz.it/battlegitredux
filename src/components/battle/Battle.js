import '../../App.css';
import React from "react";
import PlayerOne from "./players/PlayerOne";
import PlayerTwo from "./players/PlayerTwo";
import BattlePlayers from "./players/BattlePlayers";


const Battle = () => {

    return (
        <>
            <div className='row'>
                <PlayerOne />
                <PlayerTwo />
            </div>
            <div>
                <BattlePlayers />
            </div>
        </>

)


}

export default Battle;