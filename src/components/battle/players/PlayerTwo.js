import {updatePlayerTwo} from "../../../State/Battle/battle.action";
import {useDispatch, useSelector} from "react-redux";
import {PlayerInput} from "../PlayerInput";
import PlayerOne from "./PlayerOne";
import {PlayerInputTwo} from "./PlayerInputTwo";

const PlayerTwo = () => {
    const dispatch = useDispatch()
    const playerDataTwo = useSelector(state => state.battleReducer.playerDataTwo)



    const handleSubmitTwo = (id, userName) => {
        console.log("TWO SUB")
        dispatch(updatePlayerTwo({
            playerTwoName: userName,
            playerTwoImage: `https://github.com/${userName}.png?size200`,

        }))
    }

    const handleReset = (id) => {
        console.log("TWO")
        dispatch(updatePlayerTwo({
            playerTwoName: "",
            playerTwoImage: null
        }))
    }
    return (
        <div className='row'>
            {playerDataTwo.playerTwoImage ?
            <div className='column'>
                <img src={playerDataTwo.playerTwoImage} alt={"avatar"} className='avatar' />
                <h3>{playerDataTwo.playerTwoName}</h3>
                <button className='reset' onClick={()=> {handleReset('playerTwo')}}>
                    reset
                </button>
            </div> :
            <PlayerInputTwo
                id='playerTwo'
                label='player 2'
                player='Two'
                onSubmit={handleSubmitTwo}
            />}
        </div>

    )
}

export default PlayerTwo;
