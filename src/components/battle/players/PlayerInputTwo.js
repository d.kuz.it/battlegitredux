import {useDispatch, useSelector} from "react-redux";
import {updatePlayerTwo} from "../../../State/Battle/battle.action";

export const PlayerInputTwo = ({id, label, onSubmit}) => {
    const dispatch = useDispatch();
    const userName = useSelector(state => state.battleReducer.playerDataTwo.playerTwoName)
    const handleSubmit = (event) => {
        event.preventDefault()
        onSubmit(id, userName)
    }


    return (<form className='column' onSubmit={handleSubmit} >
            <label className='header' htmlFor={label}>{label}</label>
            <input
                id={label}
                type='text'
                placeholder='GitHub UserName'
                autoComplete='off'
                value={userName}
                onChange={(event) =>
                    dispatch(updatePlayerTwo({playerTwoName: event.target.value}))}/>
            <button className='button'
                    type='submit'
                    disabled={!userName.length}
            >
                Submit
            </button>
        </form>
    )
}

