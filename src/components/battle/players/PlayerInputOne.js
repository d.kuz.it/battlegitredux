import {useDispatch, useSelector} from "react-redux";
import {updatePlayerOne} from "../../../State/Battle/battle.action";

export const PlayerInputOne = ({id, label, onSubmit}) => {
    const dispatch = useDispatch();

    const userName = useSelector(state => state.battleReducer.playerDataOne.playerOneName)
    const handleSubmit = (event) => {
        event.preventDefault()
        onSubmit(id, userName)
    }


    return (<form className='column' onSubmit={handleSubmit} >
            <label className='header' htmlFor={label}>{label}</label>
            <input
                id={label}
                type='text'
                placeholder='GitHub UserName'
                autoComplete='off'
                value={userName}
                onChange={(event) =>
                    dispatch(updatePlayerOne({playerOneName: event.target.value}))}/>
            <button className='button'
                    type='submit'
                    disabled={!userName.length}

            >
                Submit
            </button>
        </form>
    )
}

