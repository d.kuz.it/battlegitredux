import {updatePlayerOne} from "../../../State/Battle/battle.action";
import {useDispatch, useSelector} from "react-redux";
import PlayerPreview from "../PlayerPreview";
import {PlayerInput} from "../PlayerInput";
import {PlayerInputOne} from "./PlayerInputOne";




const PlayerOne = () => {
    const dispatch = useDispatch()
    const playerDataOne = useSelector(state => state.battleReducer.playerDataOne)


    const handleSubmitOne = (id, userName) => {
        console.log("ONE SUB")
        dispatch(updatePlayerOne({
            [`${id}Name`]: userName,
            [`${id}Image`]: `https://github.com/${userName}.png?size200`,

        }))
    }
    const handleReset = (id) => {
        console.log("ONE")
        dispatch(updatePlayerOne({
            [`${id}Name`]: "",
            [`${id}Image`]: null
        }))
    }

        return (
            <div className='row'>
                {playerDataOne.playerOneImage ?
                    <PlayerPreview
                        avatar={playerDataOne.playerOneImage}
                        username={playerDataOne.playerOneName}
                    >
                        <button className='reset' onClick={()=> {handleReset('playerOne')}}>
                            reset
                        </button>
                    </PlayerPreview> :
                    <PlayerInputOne
                        id='playerOne'
                        label='player 1'
                        player='One'
                        onSubmit={handleSubmitOne}
                    />}
                </div>
                )
        }


export default PlayerOne;
