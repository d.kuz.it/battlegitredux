import {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import {PlayerResult} from "./PlayerResult";
import {Loader} from "../../Loader/Loader";
import {useDispatch, useSelector} from "react-redux";
import {getWinner} from "../../State/Battle/battle.thunk";


const Results = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const loading = useSelector(state => state.battleReducer.loading);
    const loser = useSelector(state => state.battleReducer.loser);
    const winner = useSelector(state => state.battleReducer.winner);

    useEffect(()=> {
        const param = new URLSearchParams(location.search);
        const playerOne = param.get('playerOneName');
        const playerTwo = param.get('playerTwoName');
        dispatch(getWinner(playerOne, playerTwo));
    }, [])

    if(loading) {
        return <Loader />
    }

  return (
      <div className='row'>

          <PlayerResult
              label='Winner'
              score={winner.score}
              profile={winner.profile}
          />
          <PlayerResult
              label='Loser'
              score={loser.score}
              profile={loser.profile}
          />
      </div>
  )
}


export default Results;