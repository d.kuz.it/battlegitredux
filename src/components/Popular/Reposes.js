import {useSelector} from "react-redux";
import {Loader} from "../../Loader/Loader";
import {Persons} from "./Persons";


export const Reposes = () => {
    const loading = useSelector(state => state.popularReducer.loading)

return <>
    {loading? <Loader /> : <Persons  />}
</>

}
