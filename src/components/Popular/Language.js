import updateLanguage from './../../State/Popular/popular.action'
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {getRepos} from "../../State/Popular/popular.thunk";
import {useSearchParams} from "react-router-dom";
let languages = ['All ','JavaScript ','Ruby ','Java ', 'Css ','Python ']

export const Language = () => {
    const dispatch = useDispatch();
    const [searchParams, setSearchParams] = useSearchParams()
    const queryParam = searchParams.get("language")
    const selectedLanguage = useSelector(state => state.popularReducer.selectedLanguage)
    const preLoading = useSelector(state => state.popularReducer.preLoading)

    localStorage.setItem("language", queryParam);

    useEffect(()=> {
        dispatch(getRepos(selectedLanguage))
        setSearchParams({language: selectedLanguage})
    },[selectedLanguage])



    return <ul className="languages">
        {languages.map((language , index) =>(
            <li key={index}
                style={{ color: language === selectedLanguage ? '#d0021b' : '#000000' }}
                className={preLoading === true ? 'menu' : ''}
                onClick={()=> dispatch(updateLanguage(language))}>
                {language}
            </li>
        ))}
    </ul>


}
