import {makeBattle} from "../../api/Api";
import {updateGetLoser, updateGetWinner, updatePlayerLoad} from "./battle.action";


export const getWinner = (playerOne, playerTwo) => (dispatch) => {


    makeBattle([playerOne, playerTwo])
        .then(([winner, loser]) => {
            dispatch(updateGetWinner(winner));
            dispatch(updateGetLoser(loser));
        })
        .finally(() => dispatch(updatePlayerLoad(false)))
}