import {fetchPopularRepos} from "../../api/Api";
import {
    getReposFailureAction,
    getReposLoadingAction,
    getReposPreLoadingAction,
    getReposSuccessAction
} from "./popular.action";

export const getRepos = (selectedLanguage) => (dispatch) => {

    dispatch(getReposLoadingAction)
    dispatch(getReposPreLoadingAction)

    fetchPopularRepos(selectedLanguage)
        .then((data) => {
            dispatch(getReposPreLoadingAction(data));
            dispatch(getReposLoadingAction(data));
            dispatch(getReposSuccessAction(data))
        }
    )
        .catch(error => dispatch(getReposFailureAction))
}