import {
    GET_REPOS_FAILURE,
    GET_REPOS_LOADING,
    GET_REPOS_PRELOADING,
    GET_REPOS_SUCCESS,
    SET_SELECTED_LANGUAGE
} from "./popular.constant";

const updateLanguage = (language) => ({
    type: SET_SELECTED_LANGUAGE,
    payload: language
})

export const getReposLoadingAction = () => ({
    type: GET_REPOS_LOADING
})

export const getReposPreLoadingAction = (payload) => ({
    type: GET_REPOS_PRELOADING,
    payload
})

export const getReposSuccessAction = (payload) => ({
    type: GET_REPOS_SUCCESS,
    payload
})

export const getReposFailureAction = (payload) => ({
    type: GET_REPOS_FAILURE,
    payload
})


export default updateLanguage;