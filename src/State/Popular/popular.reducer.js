import {
    SET_SELECTED_LANGUAGE,
    GET_REPOS_SUCCESS,
    GET_REPOS_LOADING,
    GET_REPOS_FAILURE,
    GET_REPOS_PRELOADING
} from "./popular.constant";

const initialState ={
    selectedLanguage: localStorage.getItem("language") ? localStorage.getItem("language"): 'All ',
    loading: true,
    repos: [],
    error: null,
    preLoading: true
}

const popularReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_SELECTED_LANGUAGE:
            return {
                ...state,
                selectedLanguage: action.payload,
                loading: true,
                preLoading: true,
            }

        case GET_REPOS_LOADING:
            return {
                ...state,
                error:null,
                loading: true
            }

        case GET_REPOS_PRELOADING:
            return {
                ...state,
                preLoading:true
            }

        case GET_REPOS_SUCCESS:
            return {
                ...state,
               loading: false,
                preLoading: false,
                repos: action.payload
            }
        case GET_REPOS_FAILURE:
            return {
                ...state,
                loading: false,
                preLoading: false,
                error: action.payload
            }
        default:
            return state;
    }
}
export default popularReducer;